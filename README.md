# eleventy-clock

A terribly inefficient timepiece. See https://eleventy-clock.netlify.app

![Rendering of bright red flip clock on a white surface in front of a teal wall](https://eleventy-clock.netlify.app/card.png)

## Setup

After cloning this repository, run:

```shell
npm install
```

## Development

To run a one-off build:

```shell
npm run build
```

To start a local web server and automatically reload when changes are detected:

```shell
npm run serve
```
