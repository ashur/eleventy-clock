module.exports = config =>
{
	/*
	/*
	 * Filters
	 */
	const filtersDir = './src/site/_filters';
	config.addFilter( "babel", require( `${filtersDir}/babel` ) );
	config.addFilter( "cssmin", require( `${filtersDir}/cssmin` ) );

	/*
	 * Shortcodes
	 */
	const shortcodesDir = './src/site/_shortcodes';
	config.addShortcode( "Clock", require( `${shortcodesDir}/Clock` ) );
	config.addShortcode( "Time", require( `${shortcodesDir}/Time` ) );

	/*
	 * Miscellaneous
	 */
	config.addPassthroughCopy({ "src/site/static": "/" });

	return {
		dir: {
			input: "src/site",
			output: "dist",
		},

		templateFormats: ["md", "njk"],
		markdownTemplateEngine: "njk",
	};
};
