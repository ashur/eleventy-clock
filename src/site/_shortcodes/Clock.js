module.exports = (h, m) =>
{
	return `<section class="clock">
    <div class="[][][ clock-body ][]"></div>
    <div class="[][][ clock-face ][]">
        <div class="[][][ time ][]" aria-label="The time is ${ h }:${ m }">
            <span class="[][][ number ][]">${ h }</span>
            <span class="[][][ number ][]">${ m }</span>
        </div>
    </div>
</section>`;
};
