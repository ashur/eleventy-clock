module.exports = (h, m) =>
{
    return `<span class="number">${ h }</span><span class="hidden inline-block">:</span><span class="number">${ m }</span>`;
};
