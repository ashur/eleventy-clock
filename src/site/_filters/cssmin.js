const CleanCSS = require( "clean-css" );

module.exports = string => new CleanCSS( {} ).minify( string ).styles;
