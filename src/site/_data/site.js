let origin;

if( process.env.NETLIFY )
{
	origin = process.env.DEPLOY_URL;

	if( process.env.CONTEXT === "production" )
	{
		origin = process.env.URL;
	}
}

let site = {
	title: "Eleventy Clock",
	subtitle: "A terribly inefficient timepiece",
	origin: origin,
	about: "https://multiline.co/mment/2020/09/eleventy-clock/"
};

site.metadataDescription = `${site.subtitle}. Made by Ashur Cabrera.`;

module.exports = site;
