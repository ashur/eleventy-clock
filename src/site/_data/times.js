let times = [];

let hMin = 1; let hMax = 12;
let mMin = 0; let mMax = 59;

if( process.env.TIME )
{
	let [h, m] = process.env.TIME.split( ":" );
	if( h && m )
	{
		hMin = h; hMax = h;
		mMin = m; mMax = m;
	}
}

for( let h = hMin; h <= hMax; h++ )
{
	for( let m = mMin; m <= mMax; m++ )
	{
		let time = {};

		time.h = h <= 12 ? h : h - 12
		time.m = m.toString().padStart( 2, "0" );

		// Move clock face glare ever-so-slightly over time
		let totalMin = 12 * 60;
		let currentMin = (h - 1) * 60 + m;
		time.glare = ((currentMin - totalMin) / totalMin);

		times.push( time );
	}
}

module.exports = times;
