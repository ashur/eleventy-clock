---
layout: layouts/base.njk

stylesheets:
    - clock.css
update_time: true

pagination:
    data: times
    size: 1
    alias: time
permalink: "/{{ time.h }}:{{ time.m }}/index.html"
time: time
---
<h1 class="[][][ absolute opacity:0 ][]">{{ site.title }}</h1>
{% Clock time.h, time.m %}
